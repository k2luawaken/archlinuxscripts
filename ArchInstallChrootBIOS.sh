# Arch Installer 2 - Electric Boogaloo

rm /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

echo "Installing Network and Bluetooth packages."
sudo pacman -S networkmanager bluez
sudo systemctl enable NetworkManager
sudo systemctl enable bluetooth

echo "What will your username be?"
read username

useradd -m -g wheel $username
passwd $username
echo "%wheel ALL=(ALL:ALL) ALL" >> /etc/sudoers

echo "Set the root password."
passwd

echo "Installing GRUB."
sudo pacman -S grub efibootmgr --noconfirm

lsblk
echo "Choose the drive to install GRUB on."
read grub
grub-install $grub

grub-mkconfig -o /boot/grub/grub.cfg

echo "Install finished."
